/*
Describes an immutable 2d vector allowing standard vector operations
*/
class Vec2 {
  /*
  @number x: the x value
  @number y: the y value
  */
  constructor(x, y) {
    Object.defineProperties(this, {
      x: {
        value: x,
      },
      y: {
        value: y,
      }
    });
  }

  /*
  A vector representing the negative of this vector
  #return Vec2: negated vector
  */
  get negative() {
    // memoized
    Object.defineProperty(this, 'neg',
      {
        value: new Vec2(-this.x, -this.y)
      }
    );
    return new Vec2(-this.x, -this.y);
  }

  /*
  The length or magnitude of this vector
  #return number: length
  */
  get len() {
    // memoized
    Object.defineProperty(this, 'len',
      {
        value: Math.sqrt(this.x**2 + this.y**2)
      }
    );
    return this.len;
  }

  /*
  The unit vector corresponding to this vector
  A unit vector has a length of 1
  If the vector has a length of 0, this will return (0, 0)
  #return Vec2: unit vector
  */
  get unit() {
    // memoized
    const value = this.len === 0 ? new Vec2(0, 0) : new Vec2(this.x / this.len, this.y / this.len);
    Object.defineProperty(this, 'unit', { value });
    return this.unit;
  }

  /*
  This vector represented as an angle (in radians)
  #return number: angle (in radians)
  */
  get angle() {
    Object.defineProperty(this, 'angle',
      {
        value: Math.atan2(this.y, this.x)
      }
    );
    return this.angle;
  }


  /*
  Add this vector to another
  @Vec2 v: other vector
  #return Vec2
  */
  add(v) {
    return new Vec2(this.x + v.x, this.y + v.y);
  }

  /*
  Subtract another vector from this vector
  @Vec2 v: other vector
  #return Vec2
  */
  sub(v) {
    return new Vec2(this.x - v.x, this.y - v.y);
  }

  /*
  Multiply this vector by a scalar
  @number n: scalar
  #return Vec2
  */
  mul(n) {
    return new Vec2(this.x * n, this.y * n);
  }

  /*
  Divide this vector by a scalar
  @number n: scalar
  #return Vec2
  */
  div(n) {
    return new Vec2(this.x / n, this.y / n);
  }

  /*
  Dot product of this vector and another vector
  @Vec2 v: other vector
  #return Vec2
  */
  dot(v) {
    return Vec2.sum(this.x*v.x, this.y*v.y);
  }

  /*
  Distance between two points
  @Vec2 v: other vector
  #return number: distance
  */
  dist(v) {
    return Math.abs(Math.sqrt((this.x - v.x)**2 + (this.y - v.y)**2));
  }

  /*
  Interpolate between two vectors, where 0 is this vector and 1 is the other vector
  @Vec2 v: other vector
  @number n: interpolation factor
  #return Vec2
  */
  lerp(v, n) {
    return this.add(v.sub(this).mul(n));
  }

  /*
  Sum a number of vectors
  @Vec2[] v: vectors to sum
  #return Vec2
  */
  static sum(...v) {
    return v.reduce((a, b) => new Vec2(a.x+b.x, a.y+b.y), new Vec2(0,0));
  }

  /*
  Return a unit vector that represents the angle in radians
  @number a: angle in radians
  #return Vec2
  */
  static fromAngle(a) {
    return new Vec2(Math.cos(a), Math.sin(a));
  }

  /*
  Return a random unit vector
  @return Vec2
  */
  static random() {
    return Vec2.fromAngle(Math.random() * Math.PI * 2);
  }
}

/*
A particle system that tracks and renders particles
*/
class ParticleSystem {
  #particles = {};
  #nextId = 1;

  /*
  Add a particle to the system
  @Particle particle
  */
  addParticle(particle) {
    particle._id = this.#nextId;
    this.#nextId += 1;
    this.#particles[particle._id] = particle;
    return particle._id;
  }

  /*
  Delete a particle from the system
  Usually called by the particle itself after it expires
  @Particle particle
  */
  deleteParticle(particle) {
    delete this.#particles[particle._id];
  }

  /*
  Call each particle's tick function
  */
  process() {
    for (let i in this.#particles) {
      this.#particles[i].process();
    }
  }

  /*
  Call each particle's draw function
  @number time: the current game time
  @number delta: time since the last frame
  @number tickDelta: time since the last tick
  @number interpolationFactor: amount to interpolate animation since the last tick
  */
  draw(time, delta, tickDelta, interpolationFactor) {
    for (let i in this.#particles) {
      this.#particles[i].draw(time, delta, tickDelta, interpolationFactor);
    }
  }
}


/*
Represents a particle
Should be created with `new Particle()` and then have properties configured before calling init()
init() will add the particle to the particle system and start its animation

@number ttl: time to live (seconds)
@Vec2 pos: initial position
@Vec2 vel: initial velocity (pixels/s)
@Vec2 accel: constant acceleration (pixels/s^2)
@number drag: drag factor per second
@number ang: initial angle (radians)
@number angVel: initial angular velocity (radians/s)
@number angDrag: drag factor per second
@boolean shrink: shrink at end of animation
@number shrinkAt: point of animation to start shrink (0-1)
@boolean fade: alpha fade at end of animation
@number fadeAt: point of animation to start fade (0-1)
@Vec2 size: size of particle
@Color color: color of particle
@string shape: shape of particle, ignored when texture is set ('box' | 'ellipse')
@Texture texture: texture of particle
*/
class Particle {
  /*
  @ParticleSystem particleSystem
  */
  constructor(particleSystem){
    this.#particleSystem = particleSystem;
  }
  #particleSystem;
  #startTime = null;
  #lastAng = null;
  #lastPos = null;
  _id = null;

  ttl = 0.1;
  pos = new Vec2(0, 0);
  vel = new Vec2(0, 0);
  accel = new Vec2(0, 0);
  drag = 0;
  ang = 0;
  angVel = 0;
  angDrag = 0;
  shrink = false;
  shrinkAt = 0;
  fade = false;
  fadeAt = 0;
  size = new Vec2(5,5);
  color = new Color(0,0,0);
  shape = 'box';
  texture = null;

  /*
  Process the particle's movement
  */
  process() {
    const dt = gameArea.startupValues.tickSpeed / 1000;
    this.#lastPos = this.pos;
    this.vel = this.vel.add(this.accel.mul(dt)).mul(1 - this.drag * dt);
    this.pos = this.pos.add(this.vel.mul(dt));
    this.#lastAng = this.ang;
    this.angVel = this.angVel * (1 - this.angDrag * dt);
    this.ang = this.ang + this.angVel * dt
  }

  /*
  Draw the particle
  @number time: the current game time
  @number delta: time since the last frame
  @number tickDelta: time since the last tick
  @number interpolationFactor: amount to interpolate animation since the last tick
  */
  draw(time, delta, tickDelta, interpolationFactor) {
    if (this.#lastPos == null) this.#lastPos = this.pos;
    if (this.#lastAng == null) this.#lastAng = this.ang;

    if (this.#startTime != null) {
      const t = (time - this.#startTime) / 1000;
      if (t > this.ttl) {
        this.#particleSystem.deleteParticle(this);
        return;
      }
      if (this.texture != null){
        Particle.#drawTexture(this, t, interpolationFactor);
        return;
      }
      switch(this.shape) {
        case 'ellipse':
          Particle.#drawEllipse(this, t, interpolationFactor);
          break;
        default:
        case 'box':
          Particle.#drawBox(this, t, interpolationFactor);
      }
    }
  };

  /*
  Add the particle to the particle system and start its animation
  */
  init() {
    this.#startTime = performance.now();
    this.#particleSystem.addParticle(this);
  }

  /*
  Private - draw a box particle
  @Particle particle: particle to draw
  @number t: time relative to the start of the particle's life
  @number interpolationFactor: factor to interpolate the particle's animation by
  */
  static #drawBox (particle, t, interpolationFactor) {
    const ctx = gameArea.context;
    ctx.beginPath();
    const {curColor, curSize} = Particle.#applyFadeAndShrink(particle, t);
    ctx.fillStyle = curColor.cssStyle;
    let tl = curSize.div(2).negative;
    let br = curSize.div(2);
    let tr = new Vec2(br.x, tl.y);
    let bl = new Vec2(tl.x, br.y);
    const {curAng, curPos} = Particle.#getLerpedAngPos(particle, interpolationFactor);
    tl = Vec2.fromAngle(tl.angle + curAng).mul(tl.len);
    br = Vec2.fromAngle(br.angle + curAng).mul(br.len);
    tr = Vec2.fromAngle(tr.angle + curAng).mul(tr.len);
    bl = Vec2.fromAngle(bl.angle + curAng).mul(bl.len);
    tl = tl.add(curPos);
    br = br.add(curPos);
    tr = tr.add(curPos);
    bl = bl.add(curPos);
    ctx.moveTo(tl.x, tl.y);
    ctx.lineTo(tr.x, tr.y);
    ctx.lineTo(br.x, br.y);
    ctx.lineTo(bl.x, bl.y);
    ctx.fill();
  }

  /*
  Private - draw an ellipse particle
  @Particle particle: particle to draw
  @number t: time relative to the start of the particle's life
  @number interpolationFactor: factor to interpolate the particle's animation by
  */
  static #drawEllipse (particle, t, interpolationFactor) {
    const ctx = gameArea.context;
    ctx.beginPath();
    const {curColor, curSize} = Particle.#applyFadeAndShrink(particle, t);
    ctx.fillStyle = curColor.cssStyle;
    const {curAng, curPos} = Particle.#getLerpedAngPos(particle, interpolationFactor);
    ctx.ellipse(curPos.x, curPos.y, curSize.x / 2, curSize.y / 2, curAng, 0, 2 * Math.PI);
    ctx.fill();
  }

  /*
  Private - draw a textured particle
  @Particle particle: particle to draw
  @number t: time relative to the start of the particle's life
  @number interpolationFactor: factor to interpolate the particle's animation by
  */
  static #drawTexture (particle, t, interpolationFactor) {
    const ctx = gameArea.context;
    const {curColor, curSize} = Particle.#applyFadeAndShrink(particle, t);
    const {curAng, curPos} = Particle.#getLerpedAngPos(particle, interpolationFactor);
    ctx.translate(curPos.x, curPos.y);
    ctx.rotate(curAng);
    const oldGlobalAlpha = ctx.globalAlpha;
    ctx.globalAlpha = ctx.globalAlpha * (curColor.a / 255);
    ctx.drawImage(particle.texture.image, -curSize.x / 2, -curSize.y / 2, curSize.x, curSize.y);
    ctx.globalAlpha = oldGlobalAlpha;
    ctx.rotate(-curAng);
    ctx.translate(-curPos.x, -curPos.y);
  }

  /*
  Private - used by draw functions to apply fade and shrink
  no interpolation factor cause fade and shrink are purely time based
  @Particle particle: particle to draw
  @number t: time relative to the start of the particle's life
  */
  static #applyFadeAndShrink (particle, t) {
    const fadeT = particle.fadeAt * particle.ttl;
    const fadeAmt = particle.fade && t > fadeT ? (t - fadeT)/(particle.ttl - fadeT) : 0;
    const curColor = new Color(
      particle.color.r,
      particle.color.g,
      particle.color.b,
      particle.color.a * (1 - fadeAmt)
    );
    const shrinkT = particle.shrinkAt * particle.ttl;
    const shrinkAmt = particle.shrink && t > shrinkT ? (t - shrinkT)/(particle.ttl - shrinkT) : 0;
    const curSize = particle.size.mul(1 - shrinkAmt);
    return {curColor, curSize};
  }

  /*
  Private - used by draw functions to interpolate the particle's animation
  @Particle particle: particle to draw
  @number interpolationFactor: factor to interpolate the particle's animation by
  */
  static #getLerpedAngPos(particle, interpolationFactor) {
    const curAng = lerp(particle.#lastAng, particle.ang, interpolationFactor);
    const curPos = particle.#lastPos.lerp(particle.pos, interpolationFactor);
    return {curAng, curPos};
  }
}