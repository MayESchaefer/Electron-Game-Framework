const {Point, Vector, Circle, Line, Ray, Segment, Arc, Box, Polygon, Matrix, PlanarSet, BooleanOperations} = require('@flatten-js/core');
/*
converts a geometric basic shape to a drawable Path2D
@Point, @Vector, @Line, @Ray, @Segment, @Arc, @Circle, @Box shape: the shape to be converted
*/
function shapeToPath(shape) {
  return polygonToPath(new Polygon(shape));
}

/*
converts a geometric polygon to a drawable Path2D
@Polygon polygon: the polygon to be converted
*/
function polygonToPath(polygon) {
  if (/ d="([^"]+)"/g.exec(polygon.svg()) != null) {
    return new Path2D(/ d="([^"]+)"/g.exec(
      polygon.svg()
    )[1]
      .replaceAll("\n", " ")
      .replaceAll(/ +/g, " ")
      .replace(/^ /g, ""));
  }
  else {
    return new Path2D(polygon.svg());
  }
}

function generateInterFrameHitbox(hitbox, newPosition, rotateNew) {
  let { startPoint1, startpoint2 } = getFurthestSidePointsOnPolygon(hitbox, newPosition);
  let nextHitbox = hitbox.rotate(rotateNew, hitbox.box.center).translate(new Vector(hitbox.box.center, newPosition))
  let { endPoint1, endPoint2 } = getFurthestSidePointsOnPolygon(nextHitbox, hitbox.box.center);

  let interFrameHitbox = new Polygon(startPoint1, startpoint2, endPoint1, endPoint2);
  interFrameHitbox.addFace(hitbox.edges);
  interFrameHitbox.addFace(nextHitbox.edges);

  return interFrameHitbox;
}

function getFurthestSidePointsOnPolygon(polygon, point) {
  let dir = relDirectionOfPoints(polygon.box.center, point);
  let perpendicular = dir.rotate90CW();
  let furthestBackVec = dir.clone();
      furthestBackVec.length *= polygon.box.center.distanceTo(new Point(polygon.box.xmax, polygon.box.ymax)).first;
  let furthestBack = polygon.box.center.translate(furthestBackVec);
  let furthestForward = polygon.box.center.translate(furthestBackVec.rotate(Math.PI));
  let linesSegment = new Segment(furthestBack, furthestForward);

  let furthestPoints = [polygon.box.center, polygon.box.center];

  let granularity = 10;
  for (i = 0; i < granularity; i++) {
    let lineLoc = linesSegment.pointAtLength(linesSegment.length / (i + 1));
    let line = new Line(lineLoc, perpendicular);
    let intersections = line.intersect(polygon);
    let points = [intersections.first, intersections.last];

    if (polygon.box.center.distanceTo(points.first) > polygon.box.center.distanceTo(furthestPoints.first)) {
      furthestPoints.first = points.first;
    }
    if (polygon.box.center.distanceTo(points.last) > polygon.box.center.distanceTo(furthestPoints.last)) {
      furthestPoints.last = points.last;
    }
  }

  return furthestPoints;
}

function relDirectionOfPoints(point1, point2) {
    return new Line(point1, point2).norm;
}