function init() {
  document.title = "Electron Game Framework Demo";

  createTexture('mc-flame', 'textures/mc-flame.png');

  const particleSystem = new ParticleSystem();
  gameArea.components.push(
    new Component(
      "particleSystem", 
      {}, 
      particleSystem.process.bind(particleSystem), 
      particleSystem.draw.bind(particleSystem)
    )
  );

  gameArea.components.push(
    new Component("delta", { }, null,
      function(time, delta, tickDelta, interpolationFactor) {
        ctx = gameArea.context;
        ctx.fillStyle = "black";
        ctx.font = "20px sans-serif";
        ctx.fillText("time: " + time, 10, 20);
        ctx.fillText("delta: " + delta, 10, 40);
        ctx.fillText("tick delta: " + tickDelta, 10, 60);
        ctx.fillText("interp factor: " + interpolationFactor, 10, 80);
		    ctx.fillText("fps: " + 1000/delta, 10, 100);
		    ctx.fillText("tick fps: " + accessSmooth(1000/tickDelta, 0.1, "tickfps display"), 10, 120);
		    ctx.fillText(gameArea.debug ? "debug" : "", 10, 140);
      }
    )
  );

  gameArea.components.push(
    new Component("geometry",
      { circle1: new Circle(new Point(150, 160), 30), circle2: new Circle(new Point(155, 160), 30), color1: "cyan", color2: "red", intersecting: false },
      function() {
        this.params.intersecting = this.params.circle1.intersect(this.params.circle2).length > 0;
      },
      function() {
        ctx = gameArea.context;
        ctx.fillStyle = this.params.color1;
        ctx.fill(shapeToPath(this.params.circle1));
        ctx.fillStyle = this.params.color2;
        ctx.fill(shapeToPath(this.params.circle2));
        ctx.fillStyle = "black";
        ctx.fillText(this.params.intersecting ? "intersecting" : "", 180, 160);
      }
    )
  );

  gameArea.components.push(
    new Component("move",
      { width: 30, height: 30, color: "cyan", x: 10, y: 160, speed: 5 },
        function() {
        if (gameArea.key(37) || gameArea.key(charCode("A")))
          {this.params.x += 0 - this.params.speed; }
        if (gameArea.key(39) || gameArea.key(charCode("D")))
          {this.params.x += this.params.speed; }
        if (gameArea.key(38) || gameArea.key(charCode("W")))
          {this.params.y += 0 - this.params.speed; }
        if (gameArea.key(40) || gameArea.key(charCode("S")))
          {this.params.y += this.params.speed; }
      },
      function() {
        ctx = gameArea.context;
        ctx.fillStyle = this.params.color;
        ctx.fillRect(this.params.x, this.params.y, this.params.width, this.params.height);
      }
    )
  );

  gameArea.components.push(
    new Component("clickParticle", {}, 
      function() {
        [... gameArea.cursor.clicks, ... gameArea.touch.clicks].forEach((click) => {
          const n = 125;
          for (let i = 0; i < n; i++) {
            const particle = new Particle(particleSystem);
            const v1 = new Vec2(click.start.x, click.start.y);
            const v2 = new Vec2(click.end.x, click.end.y);
            particle.pos = v1.lerp(v2, i/n + Math.random() * 1/n);
            particle.vel = Vec2.random().mul(Math.random() * 100);
            particle.accel = new Vec2(0, 200);
            particle.drag = 2;
            particle.ang = Math.random() * Math.PI * 2;
            particle.angVel = Math.random() * Math.PI * 10;
            particle.angDrag = Math.PI / 2;
            particle.ttl = Math.random() * 2;
            particle.fade = true;
            particle.fadeAt = 0.8;
            particle.shrink = true;
            particle.shrinkAt = 0.8;
            particle.size = new Vec2(15, 15);
            particle.color = new Color(255, 0, 0);
            particle.init();
          }
        });
      },
      null
    )
  );

  gameArea.components.push(
    new Component("clickTracker", {}, null,
      function() {
        gameArea.cursor.clickStarts.forEachSet(function(value) {
          let ctx = gameArea.context;

          ctx.strokeStyle = "yellow";
          ctx.beginPath();
          ctx.moveTo(value.x, value.y);
          ctx.lineTo(gameArea.cursor.x, gameArea.cursor.y);
          ctx.stroke();

          ctx.fillStyle = "blue";
          ctx.fillRect(value.x - 3, value.y - 3, 5, 5);
        }, 5);

        gameArea.cursor.clicks.forEach(function(value) {
          let ctx = gameArea.context;

          ctx.strokeStyle = "blue";
          ctx.beginPath();

          ctx.moveTo(value.start.x, value.start.y);
          if (value.drag) {
            value.drag.forEach(function(item) {
              gameArea.context.lineTo(item.x, item.y);
            });
          }
          ctx.lineTo(value.end.x, value.end.y);

          ctx.stroke();

          ctx.fillStyle = "green";
          ctx.fillRect(value.start.x - 8, value.start.y - 8, 15, 15);

          ctx.fillStyle = "red";
          ctx.fillRect(value.end.x - 8, value.end.y - 8, 15, 15);
        });

        //gameArea.cursor.clicks = [];
      }
    )
  );

  gameArea.components.push(
    new Component("touchTracker", {}, null,
      function() {
        gameArea.touch.clickStarts.forEachSet(function(value) {
          let ctx = gameArea.context;

          ctx.strokeStyle = "yellow";
          ctx.beginPath();
          ctx.moveTo(value.x, value.y);
          ctx.lineTo(gameArea.touch.x, gameArea.touch.y);
          ctx.stroke();

          ctx.fillStyle = "blue";
          ctx.fillRect(value.x - 3, value.y - 3, 5, 5);
        }, 5);

        gameArea.touch.clicks.forEach(function(value) {
          let ctx = gameArea.context;

          ctx.strokeStyle = "blue";
          ctx.beginPath();

          ctx.moveTo(value.start.x, value.start.y);
          if (value.drag) {
            value.drag.forEachSet(function(item) {
              gameArea.context.lineTo(item.x, item.y);
            });
          }
          ctx.lineTo(value.end.x, value.end.y);

          ctx.stroke();

          ctx.fillStyle = "green";
          ctx.fillRect(value.start.x - 8, value.start.y - 8, 15, 15);

          ctx.fillStyle = "red";
          ctx.fillRect(value.end.x - 8, value.end.y - 8, 15, 15);
        });

        //gameArea.touch.clicks = [];
      }
    )
  );

  let nextFlameTime = null;

  gameArea.components.push(
    new Component("mouse",
      { width: 5, height: 5, x: 10, y: 120 },
      null,
      function(time) {
        this.params.x = gameArea.cursor.x - 3;
        this.params.y = gameArea.cursor.y - 3;

        ctx = gameArea.context;
        if (gameArea.cursor.click) {
          ctx.fillStyle = "magenta";
        }
        else {
          ctx.fillStyle = "black";
        }
        ctx.fillRect(this.params.x, this.params.y, this.params.width, this.params.height);

        if (nextFlameTime == null || time >= nextFlameTime){
          nextFlameTime = time + 500 + Math.random() * 2000;
          const particle = new Particle(particleSystem);
          const cursorPos = new Vec2(gameArea.cursor.x, gameArea.cursor.y);
          particle.pos = cursorPos.sub(new Vec2(0, 10)).add(Vec2.random().mul(Math.random()*10));
          particle.vel = particle.pos.sub(cursorPos).unit.mul(Math.random() * 25);
          particle.accel = new Vec2(0, -100);
          particle.ttl = 0.3 + Math.random() * 1;
          particle.fade = true;
          particle.shape = 'ellipse';
          const flameTex = gameArea.textures['mc-flame'];
          particle.size = new Vec2(flameTex.image.naturalWidth, flameTex.image.naturalHeight).mul(5);
          particle.texture = flameTex;
          particle.init();
        }

        ctx.fillStyle = "black";
        ctx.font = "20px sans-serif";
        ctx.fillText("cursor location: [" + this.params.x + ", " + this.params.y + "]", 200, 20);
      }
    )
  );

  gameArea.components.push(
    new Component("keys",
      { x: 10, y: 700 },
      null,
      function() {
        let keyboardOutput = "";
        gameArea.keyboard.keys.forEach(function(value) {
          keyboardOutput += String.fromCharCode(value);
        });

        if (keyboardOutput != "") {
          ctx = gameArea.context;
          ctx.fillStyle = "black";
          ctx.font = "20px sans-serif";
          ctx.fillText(keyboardOutput + " (" + [...gameArea.keyboard.keys].join(' ') + ")", this.params.x, this.params.y);
        }
      }
    )
  );

  gameArea.start();
}
